let listTratamientos = [{value:0, name: 'Centro de dia jornada doble',files:
[{
    name:'Pedido Médico ',
    nameTag:'pedioMedico',
    obligatorio:true
},
{
    name:'Plan de tratamiento ',
    nameTag:'planTratamiento',
    obligatorio:true
},
{
    name:'Presupuesto ',
    nameTag:'presupuesto',
    obligatorio:true
},
{
    name:'Consentimiento Informado	',
    nameTag:'consentimiento',
    obligatorio:true
},
{
    name:'Conformidad SSS ',
    nameTag:'conformidad',
    obligatorio:true
},
{
    name:'Escala FIM ',
    nameTag:'conformidad',
    obligatorio:false
}

]},
// {value:1, name: 'Centro de dia jornada simple'},
// {value:2, name: 'Centro educativo terapeutico jornada doble '},
// {value:3, name: 'Centro educativo terapeutico jornada simple '},
// {value:4, name: 'Formacion laboral jornada doble '},
// {value:5, name: 'Formacion laboral jornada simple '},
// {value:6, name: 'Aprestamiento laboral jornada doble '},
// {value:7, name: 'Aprestamiento laboral jornada simple '},
// {value:8, name: 'Educacion inicial jornada doble (Educación Pre- Primaria) '},
// {value:9, name: 'Educacion inicial jornada simple (Educación Pre- Primaria) '},
// {value:10, name: 'EGB (Escuela general basica) jornada doble (Educación Primaria) '},
// {value:11, name: 'EGB (Escuela general basica) jornada simple (Educación Primaria) '},
// {value:12, name: 'Hogar permanente '},
// {value:13, name: 'Hogar de lunes a viernes '},
// {value:14, name: 'Hogar con Centro de Dia de Lunes a Viernes '},
// {value:15, name: 'Hogar con CET de Lunes a Viernes '},
// {value:16, name: 'Hogar de Lunes a viernes con Formación Laboral '},
// {value:17, name: 'Hogar de Lunes a viernes con Pre- Primaria '},
// {value:18, name: 'Hogar de Lunes a Viernes con Primaria '},
// {value:19, name: 'Hogar con CET Permanente '},
// {value:20, name: 'Hogar con Centro de Dia Permanente '},
// {value:21, name: 'Hogar Permanente con Formación Laboral '},
// {value:22, name: 'Hogar Permanente con Pre- Primaria '},
// {value:23, name: 'Hogar Permanente con Primaria '},
// {value:24, name: 'Pequeño Hogar de Lunes a Viernes '},
// {value:25, name: 'Pequeño Hogar Permanente'},
// {value:26, name: 'Residencia de Lunes a Viernes'},
// {value:27, name: 'Residencia Permanente'},
// {value:28, name: 'Estimulacion temprana'},
// {value:29, name: 'Estimulacion auditiva'},
// {value:30, name: 'Estimulacion visual'},
// {value:31, name: 'Fonoaudiologia'},
// {value:32, name: 'Hidroterapia'},
// {value:33, name: 'Kinesiologia'},
// {value:34, name: 'Musicoterapia'},
// {value:35, name: 'Neurolinguistica'},
// {value:36, name: 'Psicologia'},
// {value:37, name: 'Psicomotricidad'},
// {value:38, name: 'Psicopedagogia'},
// {value:39, name: 'Rehabilitacion de la deglucion'},
// {value:40, name: 'Terapia ocupacional'},
{value:1, name: 'Módulo de maestro de apoyo', files:[{
name:'Pedido Médico ',
nameTag:'pedioMedico',
obligatorio:true
},
{
name:'Plan de tratamiento ',
nameTag:'planTratamiento',
obligatorio:true
},
{
name:'Presupuesto ',
nameTag:'presupuesto',
obligatorio:true
},
{
name:'Consentimiento Informado	',
nameTag:'consentimiento',
obligatorio:true
},
{
name:'Conformidad SSS ',
nameTag:'conformidad',
obligatorio:true
},
{
name:'Certificado de alumno regular ',
nameTag:'certificadoRegular',
obligatorio:false
},
{
name:'Acta Acuerdo ',
nameTag:'actaAcuerdo',
obligatorio:false
}
]}
// ,
// {value:42, name: 'Módulo de Apoyo a la integracion escolar '},
// {value:43, name: 'Rehabilitación- Hospital de día Jornada Simple '},
// {value:44, name: 'Rehabilitación- Hospital de día jornada Doble '},
// {value:45, name: 'Rehabilitacion en internacion '},
// {value:46, name: 'Alimentacion '},
// {value:47, name: 'Transporte '},
// {value:48, name: 'Acompañamiento Externo '},
// {value:49, name: 'Acompañante terapeutico '},
// {value:50, name: 'Equinoterapia '},
// {value:51, name: 'Estimulacion Cognitiva '},
// {value:52, name: 'Estimulacion Kinesica '},
// {value:53, name: 'Habilidades Sociales '},
// {value:54, name: 'Hospital de Dia '},
// {value:55, name: 'Orientacion a Padres '},
// {value:56, name: 'Tratamiento Cognitivo Conductual '}
]

export default listTratamientos