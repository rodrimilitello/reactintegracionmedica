import React, { useState } from 'react'
import '../asset/css/test.css'
import '../components/Archivo'
import Archivo from '../components/Archivo'
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';



const Tratamiento = (props) => {

   
    const [archivos,setArchivos] = useState(props.files)
    const otroHandler = () => {
        let otroName =archivos.length + 1
        otroName ='otro'+otroName
        let otroArchivo = [...archivos,
                            {
                                name:'Otros ',
                                nameTag:otroName,
                                obligatorio:false
                            }
                        ]
        setArchivos(otroArchivo)
    }
    
    return(
        <div className=''>
            <Accordion defaultExpanded={true}>
                <AccordionSummary>
            <h3>{props.name}</h3>
                </AccordionSummary>

            <div className='archivos' >
                {archivos.map((archivo, index)=><Archivo name={archivo.name} key={index} tagName={archivo.nameTag+props.index} obligatorio={archivo.obligatorio}/>)}
            </div>
       
            <button onClick={props.delete} className='boton-gris'>Borrar</button>
            <button onClick={otroHandler}className='boton-azul'>Otro</button>
            </Accordion>
        </div>
    )
}

export default Tratamiento