import React, { useState } from 'react'
import '../asset/css/test.css'
import { DataGrid } from '@material-ui/data-grid';



const filas = [
  { id: 1, apellido: 'Militello', nombre: 'Rodrigo', dni: '31343234', nroSocio: '23-31343234',pendiente:'Si' },
  { id: 2, apellido: 'Santillan', nombre: 'Marcelo', dni: '31343234', nroSocio: '23-31343234',pendiente:'Si' },
  { id: 3, apellido: 'Lopez', nombre: 'Jonnathan', dni: '31343234', nroSocio: '23-31343234',pendiente:'No' },
  { id: 4, apellido: 'Monzon', nombre: 'Jaqueline', dni: '31343234', nroSocio: '23-31343234',pendiente:'No' },
  { id: 5, apellido: 'Brac', nombre: 'Ivan', dni: '31343234', nroSocio: '23-31343234',pendiente:'Si' },
  { id: 6, apellido: 'Brac', nombre: 'Ivan', dni: '31343234', nroSocio: '23-31343234',pendiente:'No' },
  { id: 7, apellido: 'Reynoso', nombre: 'Daniela', dni: '31343234', nroSocio: '23-31343234',pendiente:'No' },
  { id: 8, apellido: 'Cadenas', nombre: 'Gaston', dni: '31343234', nroSocio: '23-31343234',pendiente:'No' },
  { id: 9, apellido: 'Noelia', nombre: 'Caldera', dni: '31343234', nroSocio: '23-31343234',pendiente:'No' },
  { id: 10, apellido: 'Agamenhoni', nombre: 'Lucia', dni: '40784584', nroSocio: '23-40784584',pendiente:'No' },
  { id: 11, apellido: 'Mauri', nombre: 'Marcos', dni: '30789345', nroSocio: '23-30789345',pendiente:'No' },
  { id: 12, apellido: 'Ramirez', nombre: 'Nicolas', dni: '34879346', nroSocio: '23-34879346',pendiente:'No' },
  { id: 13, apellido: 'Esposito', nombre: 'Norberto', dni: '37463548', nroSocio: '23-37463548',pendiente:'No' },
];

const columns = [
  { field: 'apellido', headerName: 'Apellido', width: 150, flex: 1, sortable: false, headerAlign: 'center', align: 'center' },
  { field: 'nombre', headerName: 'Nombre', width: 150, flex: 1, sortable: false, headerAlign: 'center', align: 'center' },
  { field: 'dni', headerName: 'DNI', width:150, flex: 1, sortable: false, headerAlign: 'center', align: 'center' },
  { field: 'nroSocio', headerName: 'Nro Socio', width: 150, flex: 1, sortable: false, headerAlign: 'center', align: 'center' },
  { field: 'pendiente', headerName: 'Pendiente', width: 150, flex: 1, sortable: false, headerAlign: 'center', align: 'center' },
];

const ListaSocios = () => {
  const [rows, setRows] = useState(filas)
    return(
        <div>
            <div style={container} >
                <h2 >Lista de Socios</h2>
            </div>
            <div  style={{ height: 900, width: "50%", display:'flex' , justifyContent: "center"}}>
                <DataGrid 
                    rows={rows} 
                    columns={columns}  
                    disableColumnMenu/>
            </div>
        </div>
    )
}

const container = {
    height: "100%",
    width: "100%",
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
  }

export default ListaSocios