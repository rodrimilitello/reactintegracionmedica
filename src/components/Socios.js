import React from 'react';
import '../asset/css/test.css';
import ListaSocios from './ListaSocios';
import BuscadorSocios from './BuscadorSocio';

const Socios = () => {
    return (
        <div>
            <BuscadorSocios/>
            <ListaSocios/>
        </div>
    )
}

export default Socios;