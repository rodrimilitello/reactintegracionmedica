import React from 'react'
import '../asset/css/test.css'
import '../asset/css/DatosSocios.css'
import MarcaDisca from '../asset/images/disca.svg'

const DatosSocios = () =>{

    return(
        <div className='DatoSocioContainer'>
            <h2>Datos del Socio</h2>
            <div className='DatoSocio'>

            <p>
<label htmlFor='nombre'><b>Nombre: </b></label> <span className='' id='nombre'>Juan Carlos</span>
            </p>
            <p>
<label htmlFor='Apellido'><b>Apellido: </b></label> <span className='' id='Apellido'>Perez</span>
            </p>
            <p>
<label htmlFor='dni'><b>DNI: </b></label> <span className='' id='dni'>34987654</span>
            </p>
            <p>
<label htmlFor='nroSocio'><b>Nro Socio: </b></label> <span className='' id='nroSocio'>6012345601</span>
            </p>
            <p>
<label htmlFor='plan'><b>Plan: </b></label> <span className='' id='plan'>2 210</span>
            </p>
            <p>
<label htmlFor='oSocial'><b>Obra Social: </b></label> <span className='' id='oSocial'>Osde</span>
            </p>
            <p>
<label htmlFor='marcas'><b>Marcas: </b></label> <img className='marca' id='marcas'src={MarcaDisca}/>
            </p>
            
            </div>
        </div>
    )

}

export default DatosSocios