import React from 'react'
import '../asset/css/test.css'
import '../asset/css/DocumentacionGeneral.css'
import Archivo from './Archivo'

const DocumentacionGeneral = () => {
    return(
        <div className='tratamientosContenedor'>
            <h2>Documentacion General del Socio</h2>
            <div className=' archivos'>
            <Archivo tagName='certDisca' name='Certificado de Discapacidad: ' obligatorio={false}/>
            <Archivo tagName='histClinica' name='Historia clinica: ' obligatorio={false}/>
            {/* <button className='boton-azul'>Guardar</button> */}
            </div>
        </div>
    )
}

export default DocumentacionGeneral