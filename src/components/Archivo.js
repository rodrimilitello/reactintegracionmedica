import React, { Fragment, useState } from 'react'
import trash from '../asset/images/trash.svg'
import '../asset/css/Archivo.css'


const Archivo = (props) => { 

    const [fileName,setFileName] =useState('')
    const [uso,setUso]=useState(false)

    const fileChangeHandler = (event) => {
        let verdadero = true
        setUso(verdadero)
        let path = event.target.value
        path = path.split('\\')
        setFileName(path[path.length-1])
    }
    
    let obligatorio=''
    let trash= ''
    console.log(uso)

    if(uso){
        trash = <img className='marca' id='marcas'src={trash}/> 
    }

    if(props.obligatorio){
        obligatorio= <span className="optativo">obligatorio* </span>
    }

return(
    <Fragment>
        <div className='archivoInput'>

        {/* <p className='ArchivoContenedor'> */}
        {obligatorio}
        <label><b>{props.name}</b></label>
        <label htmlFor={props.tagName} className="custom-file-upload">
        +
        </label>
        </div>
        <div className='archivoLabel'>
            
        <input id={props.tagName} type="file" onChange={fileChangeHandler}/>
        <span> {fileName} </span>
        {trash}
        </div>
     
        {/* </p> */}
    </Fragment>
)
}

export default Archivo