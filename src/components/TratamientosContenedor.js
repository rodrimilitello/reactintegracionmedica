import React,{ useState } from 'react'
import '../asset/css/test.css'
import '../asset/css/TratamientosContenedor.css'
import Tratamiento from './Tratamiento'
import listTratamientos from '../mock/tratamientosMock'
import { Link } from 'react-router-dom'


const TratamientosContenedor = ()=>{
    const [tratamientos, setTratamientos] = useState([])
    const [nuevoTratamiento, setNuevoTratamiento] = useState([])
    
    const nuevoTratamientoHandler = (event) =>{
        event.preventDefault()
        console.log(nuevoTratamiento)
        if(nuevoTratamiento!=''){
            let tratamientosNew = [...tratamientos,
                nuevoTratamiento
                ];
                console.log(tratamientosNew)
                setTratamientos(tratamientosNew);
            } 
            console.log(tratamientos)
    }

    const deleteTratamientoHandler = (index) => {
        let tratamientosNew = [...tratamientos]
        tratamientosNew.splice(index,1)
        setTratamientos(tratamientosNew);
    }

    const selectHandler = (event) =>{
        let trata = listTratamientos[event.target.value]
        setNuevoTratamiento(trata)
    }

    return(
        <div className='tratamientosContenedor'>
            <h2>Tratamientos</h2>
            <form >

            <select className='campo' id='tramite' onChange={selectHandler} defaultValue='Seleccione'>
                <option>Seleccione Tipo de tratamiento</option>
                {listTratamientos.map((t)=><option key={t.value} value={t.value}>{t.name} </option>)}             
            </select>
            <button 
                className='boton-azul'
                onClick={nuevoTratamientoHandler}
                >Nuevo tratamiento
            </button>
                    </form>

                <div>
                    {tratamientos.map((tratamiento, index) =>
                        <Tratamiento 
                        name={tratamiento.name} 
                        key={index} 
                        index={index}
                        files={tratamiento.files}
                        delete={deleteTratamientoHandler.bind(this,index)}
                        />
                        )}
                </div>
            <Link to='/index'>
                <button className='boton-azul'>Guardar</button>
            </Link>
            <Link to='/index'>
                <button className='boton-azul'>Guardar y Enviar</button>
            </Link>
        </div>
    )
}

export default TratamientosContenedor