import React from 'react';
import '../asset/css/test.css';
import '../asset/css/Detalle.css'
import DatosSocios from './DatosSocios';
import DocumentacionGeneral from './DocumentacionGeneral';
import TratamientosContenedor from './TratamientosContenedor';

const Detalle = ()=>{
    return (
    <div className="DetalleContenedor ">
        <DatosSocios/>
        <hr/>
        <DocumentacionGeneral/>
        <hr/>
        <TratamientosContenedor/>  
    </div>
    ) 
}

export default Detalle;