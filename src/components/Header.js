import React from 'react'
import logo from '../asset/images/osde.svg'
import '../asset/css/test.css'
import '../asset/css/Header.css'



const Header = () => {
    return (
        <header className='Header'>
          <div className='horizontal '>
            <img src={logo} className='logo-osde' alt='logo-osde'/>
            <h3 className='titulo-osde'>Integracion medica</h3>
          </div>
          <div className='vertical'>
            <div>
              <label htmlFor='version'>Version: </label>
              <span id='version'>0.0.1-Beta</span>
            </div>
            <div>
              <label htmlFor='prestador'>Prestador: </label>
              <span id='prestador'>Nombre del prestador</span>
            </div>
              <button className='boton-azul'id='cerrarSesion'>Cerrar Sesion</button>
          </div>
          
        </header>
    )
}

export default Header