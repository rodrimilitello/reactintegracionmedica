import Header from './components/Header';
import Socios from './components/Socios';
import Detalle from './components/Detalle';
import { BrowserRouter as Router,Switch,Route } from "react-router-dom";

const App = () => {
  return (
      <div className="App">
      <Header/>
      <Router>
        <Switch>
          <Route path='/index'> <Socios/> </Route>
          <Route path='/detalle'><Detalle/></Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
